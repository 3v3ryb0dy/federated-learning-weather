import os
import json
import numpy as np
import tensorflow as tf
import tensorflowjs as tfjs

from dotenv import load_dotenv
from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS, cross_origin




class Wrapper():
    weight_set = []


wrapper = Wrapper()
load_dotenv()
MODEL_PATH_PY = os.getenv("MODEL_PATH_PY")
MODEL_PATH_PY_UNTOUCHED = os.getenv("MODEL_PATH_PY_UNTOUCHED")
MODEL_PATH_JS = os.getenv("MODEL_PATH_JS")
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'




@app.route("/updateWeights", methods=["GET"])
@cross_origin()
def updateWeights():
    model = tf.keras.models.load_model(MODEL_PATH_PY)
    print(model.summary())
    return "201"


if __name__ == "__main__":
    app.run(debug=True)
